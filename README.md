# Welcome to the Meltwater Labs Interviews 2019

On this page you will find instructions to complete the test for this part of your interview.



INSTRUCTIONS

1. All the data you need for this exercise can be found here -> https://api.myjson.com/bins/1eenaa

2. Make a network request to get this data into the app you will build.

3. For your task you have to model this data as you see fit. Each item in the data is necessary to complete this exercise.

4. Create a ListView that represents the data you have saved. Your finished app should look like the screenshots below.

 EXAMPLE IMAGE 1
 
 
![ListView](https://bitbucket.org/nkutse/mlabs-interviews/downloads/1.png)


EXAMPLE IMAGE 2


![ListView2](https://bitbucket.org/nkutse/mlabs-interviews/downloads/6.png)


EXAMPLE IMAGE 3 
 
 
![ListView](https://bitbucket.org/nkutse/mlabs-interviews/downloads/3.png)


EXAMPLE IMAGE 4


![ListView2](https://bitbucket.org/nkutse/mlabs-interviews/downloads/2.png)
